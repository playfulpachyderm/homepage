#!/usr/bin/env bash

# == Postgres database

sudo -u postgres psql -lqt | cut -d \| -f 1 | grep -qw homepage
if [[ $? != 0 ]]
then
	echo "Creating database"
	sudo -su www-data
	sudo -u postgres psql -c "create user \"www-data\" with password 'i am breitbart'"
	sudo -u postgres psql -c "create database homepage with owner \"www-data\""
	sudo -u "www-data" psql -d homepage -f /app/homepage/db/schema.sql -a
fi
