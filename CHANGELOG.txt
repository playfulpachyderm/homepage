2.3.0
-----

Date: Dec 15, 2019

REFACTOR (chef): moved a bunch of project-specific configs from apache2, postgres etc. cookbooks to that project cookbook
REFACTOR (chef): cleaned up apache2 cookbook a bit, splitting SSL into its own recipe
FEATURE (chef): created pip and virtualenv resouce types
FEATURE: New project mini service, `/dump`

2.2.0
-----

Date: Apr 14, 2019

FEATURE (chef): install pgadmin
BUG (newscraper): updated based on a change to Yahoo's page design
MISC (chef): moved some homepage-specific configuration from `apache2` to the `homepage` cookbook

2.1.0
-----

Date: Feb 23, 2019

FEATURE (UI): added favicon
FEATURE (chef): SSL certs are now handled by chef
FEATURE (chef): chef now is aware of what environment it's running in (dev vs production)
BUG (UI): fixed stock tiles not resizing text properly based on window size
BUG (db): fixed some errors in the SQL initializers

2.0.0
-----

Date: Nov 25, 2018

- UPGRADE: Ubuntu version 16.04 to 18.04
- UPGRADE: Python version 3.5 to 3.6
- UPGRADE: Views now use Jinja2 as a template engine instead of string formatting
- FEATURE: Reworked interaction between `refresh_tiles.py`, views, scrapers and database
	- See wiki page for details
- FEATURE: scrapes data from Yahoo instead of AlphaVantage
- FEATURE: added commmodity and index tile types
- FEATURE: tiles now indicate if their data is outdated
- FEATURE (db): new `homepage/db` folder for all database-related stuff
- FEATURE (db): database is now (sort of) versioned (`schema-changes`)
- FEATURE (UI): Tiles are slightly smaller and now fit 5 per row
- FEATURE (chef): Chef repo is now a submodule
- FEATURE (chef): introduced nosetests
- BUG (chef): fixed Python installation
- BUG (chef): fixed idempotency issue involving tile generation and tile names

1.1.0
-----

Date: Oct 28, 2018

- UPGRADE: changed `psycopg2` package to `psycopg2-binary`
- FEATURE: Added "Refresh Tiles" button
- FEATURE: Tiles generated for every stock in the database
- REFACTOR: Extracted out some clean functions
- DOCUMENTATION: Added docstrings to newscrapers
- MISC: moved `img`, `views` and `newscraper` into `homepage` directory

1.0.0
-----

Date: Oct 19, 2018

- FEATURE: Added HTTPS
- FEATURE: Page now has a title

0.1.0
-----

Date: Sept 15, 2018

- FEATURE: Added `/img` folder to site to serve images
- FEATURE (UI): Created CSS file
	- Tiles now display as actual tiles instead of paragraphs

0.0.1
-----

Date: May 24, 2018

- BUG: fixed tiles being owned by root by default, and thus unaccessible to www-data
- FEATURE: System clock now syncs with Toronto

0.0.0
-----

Date: May 19, 2018

Initial release.
