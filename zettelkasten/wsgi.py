from flask import Flask, render_template, abort, request

application = Flask(__name__)

@application.route("/")
def index():
    print(request.url)
    return """
<html>
<body>
    <form action="/zettelkasten" method="post">
        <textarea name="contents"></textarea>
        <input type="submit" value="yes"/>
    </form>
</body>
</html>
"""


@application.route("/", methods=['POST'])
def post():
    file = open("/var/log/the-dump.txt", 'a')
    file.write(request.form['contents'])
    file.write("\n")
    file.close()

    return index()


if __name__ == "__main__":
    application.run()
