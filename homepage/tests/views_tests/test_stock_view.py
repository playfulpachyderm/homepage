from nose.tools import *  # pylint: disable=wildcard-import,unused-wildcard-import
import re


import sys
sys.path += ["/app/homepage/views"]
import stock_view

maxDiff = None

def assert_whitespace_equal(a, b):
    def canonical(string):
        return re.sub("\\s+", " ", string).strip()
    return assert_equal(canonical(a), canonical(b))

def test_format_data():
    data = ('Telus', 'TSE:T', 45.96, 'CAD', 45.88, 1541878528.0, 45.93, False)
    formatted = stock_view.format_data(data)

    assert_equal("Telus", formatted["name"])
    assert_equal("TSE:T", formatted["symbol"])
    assert_equal(45.96, formatted["bought_at"])
    assert_equal("", formatted["currency"])
    assert_equal(45.88, formatted["latest_price"])
    assert_equal(45.93, formatted["prev_close"])
    assert_equal(False, formatted["outdated"])
    assert_almost_equal(-0.05, formatted["change"])
    assert_almost_equal(-0.08, formatted["change_from_purchase"])
    assert_equal("Nov 10, 2018 at 14:35", formatted["formatted_timestamp"])


formatted = {
    'bought_at': 45.96,
    'latest_price': 45.88,
    'change': -0.04999999999999716,
    'change_from_purchase': 0.0799999999999983,  # Sign flipped for testing
    'currency': '',
    'direction_of_change': 'red',
    'direction_of_purchase_change': 'red',
    'formatted_timestamp': 'Nov 10, 2018 at 14:35',
    'last_scraped': 1541878528.0,
    'name': 'Telus',
    'outdated': True,
    'prev_close': 45.93,
    'symbol': 'TSE:T'
}

expectation = """
<div class="tile stock-tile">
  <div class="tile-contents">
    <div class="title-area stock-title-area">
      <h1 class="stock-name">Telus</h1>
      <div class="stock-symbol">[TSE:T]</div>
    </div>
    <div class="content-area stock-content-area">
      <div class="content-holder stock-content-holder">
        <div class="stock-current">$45.88</div>
        <div class="stock-change">-0.05</div>
        <div class="stock-purchase-change">+0.08 from purchase</div>
        <div class="stock-timestamp">Outdated (Nov 10, 2018 at 14:35)</div>
      </div>
    </div>
  </div>
</div>
"""

def test_stock_view():
    x = formatted.copy()
    x["outdated"] = False
    assert("stock-timestamp" not in stock_view.main(x))

def test_outdated_stock_view():
    assert_whitespace_equal(expectation, stock_view.main(formatted))
