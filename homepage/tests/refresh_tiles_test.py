import datetime

from nose.tools import *  # pylint: disable=wildcard-import,unused-wildcard-import

import sys
sys.path += ["/app/homepage"]
import refresh_tiles  # pylint: disable=import-error


def test_tilename_for_stock():
    stock = dict(id=1, name="Asdf Inc", symbol="ASDF", bought_at=10, currency=1, latest_price=11, last_scraped=datetime.datetime(2018, 10, 10))
    assert_equal("asdf_inc_tile", refresh_tiles.tilename_for(stock))
    stock["name"] = "Asdf & Sons Inc"
    assert_equal("asdf_sons_inc_tile", refresh_tiles.tilename_for(stock))
    stock["name"] = "Asdf (plus a few) Inc"
    assert_equal("asdf_plus_a_few_inc_tile", refresh_tiles.tilename_for(stock))
