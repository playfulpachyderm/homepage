import io
import datetime

import unittest.mock as mock
from nose.tools import *  # pylint: disable=wildcard-import,unused-wildcard-import

import sys
sys.path += ["/app/homepage/newscraper"]
import dowjones  # pylint: disable=import-error
from api_error import APIError  # pylint: disable=import-error

api_data = b"""{
    "Meta Data": {
        "1. Information": "Daily Prices (open, high, low, close) and Volumes",
        "2. Symbol": "WHF",
        "3. Last Refreshed": "2018-10-26 14:53:07",
        "4. Output Size": "Compact",
        "5. Time Zone": "US/Eastern"
    },
    "Time Series (Daily)": {
        "2018-10-26": {
            "1. open": "12.7900",
            "2. high": "12.8000",
            "3. low": "12.6800",
            "4. close": "12.7500",
            "5. volume": "24725"
        },
        "2018-10-25": {
            "1. open": "12.7300",
            "2. high": "12.9200",
            "3. low": "12.7200",
            "4. close": "12.8500",
            "5. volume": "37391"
        },
        "2018-10-24": {
            "1. open": "12.9700",
            "2. high": "13.0000",
            "3. low": "12.7200",
            "4. close": "12.7200",
            "5. volume": "29176"
        },
        "2018-10-23": {
            "1. open": "12.9200",
            "2. high": "13.0300",
            "3. low": "12.7200",
            "4. close": "13.0000",
            "5. volume": "38315"
        }
    }
}
"""

def test_expected_timestamp_weekday():
    assert_equal("2018-10-15", dowjones.expected_timestamp(datetime.date(2018, 10, 15)))  # Monday
    assert_equal("2018-10-16", dowjones.expected_timestamp(datetime.date(2018, 10, 16)))  # Tuesday
    assert_equal("2018-10-17", dowjones.expected_timestamp(datetime.date(2018, 10, 17)))  # Wednesday
    assert_equal("2018-10-18", dowjones.expected_timestamp(datetime.date(2018, 10, 18)))  # Thursday
    assert_equal("2018-10-19", dowjones.expected_timestamp(datetime.date(2018, 10, 19)))  # Friday


def test_expected_timestamp_weekend():
    assert_equal("2018-10-19", dowjones.expected_timestamp(datetime.date(2018, 10, 20)))  # Saturday
    assert_equal("2018-10-19", dowjones.expected_timestamp(datetime.date(2018, 10, 21)))  # Sunday

def test_get_data_parses_valid_response():
    def fake_date():
        return datetime.date(2018, 10, 26)
    def fake_data(_):
        return io.BytesIO(api_data)

    with mock.patch.object(dowjones.request, "urlopen", fake_data):
        with mock.patch.object(dowjones, "current_date", fake_date):
            results = dowjones.get_data()
            for key in ["open", "close", "high", "low", "volume", "timestamp", "prev_close"]:
                assert_in(key, results)

@raises(APIError)
def test_get_data_handles_throttling_properly():
    def fake_data(_):
        return io.BytesIO(b'{"Information": "Your lookup failed LOL"}')

    with mock.patch.object(dowjones.request, "urlopen", fake_data):
        dowjones.get_data()
