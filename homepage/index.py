#!/usr/bin/env python

import time
import functools
from subprocess import run, PIPE
import cgi

import psycopg2

import sys
sys.path += ["/app/homepage/db"]
import queries  # pylint: disable=import-error

from refresh_tiles import tilename_for


run = functools.partial(run, stdout=PIPE, stderr=PIPE)

# DB-connect
conn = psycopg2.connect(dbname="homepage", user="www-data", password="i am breitbart", host="localhost")

def whoami():
    return run("whoami").stdout.decode().strip()

def get_visit_count():
    cur = conn.cursor()
    we_are = cur.execute("select visits from visit_count where id=1")
    ret = cur.fetchone()[0]
    cur.close()
    return ret

def update_if_needed():
    form = cgi.FieldStorage()
    if "increment" in form and form["increment"]:
        cur = conn.cursor()
        cur.execute("update visit_count set visits = visits+1 where id=1")
        print("updating")
        conn.commit()

def load_tile(tilename):
    path = "/var/cache/www/tiles/{}.html".format(tilename)
    contents = open(path).read().strip() or """<div class="tile btc-tile">
    <div class="tile-contents">
        [Tile broken]
    </div>
</div>"""

    return contents

print("Content-Type: text/html")
print()
print("""
<html>
<head>
    <meta charset="utf-8" />
    <link rel="icon" href="/img/bitcoin-logo.png" />
    <link rel="stylesheet" type="text/css" href="/css/tile.css" />
    <title>The Homepage</title>
</head>
<body>
""")
print("<h1>Today is {}.</h1>".format(time.strftime("%b %-d, %Y (%a).  It is %H:%M")))
print("""<form action="/refresh_tiles">
    <input type="submit" value="Refresh tiles" />
</form>
<div><a href="/zettelkasten">Zettelkasten</a></div>""")

update_if_needed()

print(load_tile("bitcoin_tile"), end="")

for index in queries.get_all_indexes():
    print(load_tile(tilename_for(index)), end="")

for stock in queries.get_all_stocks():
    print(load_tile(tilename_for(stock)), end="")

for commodity in queries.get_all_commodities():
    print(load_tile(tilename_for(commodity)), end="")

print("</body></html>")

# nytimes_info = nytimes.get_todays_data()
# nytimes_info.pprint()
