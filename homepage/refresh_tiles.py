#!/usr/bin/env python

import re
from subprocess import run
from multiprocessing import Pool
import sys
import traceback

sys.path += ["/app/homepage/newscraper", "/app/homepage/db"]
import yahoo_scraper  # pylint: disable=import-error
import queries  # pylint: disable=import-error


def refresh_tile(tile, view, *args):
    run_cmd = "/app/homepage/views/{}.py".format(view)

    with open("/var/cache/www/tiles/{}.html".format(tile), "w") as out:
        try:
            run([run_cmd] + list(args), stdout=out)
        except Exception as e:
            raise RuntimeError(run_cmd + ", ".join(args)) from e


def tilename_for(stock):
    """Helper function"""
    name = re.sub("[\\s&()]+", "_", stock["name"].strip().lower())
    return name + "_tile"


def refresh_stock(stock):
    """
    Performs the complete cycle of updating a stock.
    1) calls newscraper to update data
    2) writes data to DB
    3) triggers views
    """
    try:
        # Call newscraper
        data = yahoo_scraper.get_stock(stock["symbol"])
    except:
        # Scrape failed; data is now outdated
        sys.stderr.write("The following error occurred while processing stock {}:\n".format(stock["symbol"]))
        traceback.print_exc()
        queries.set_stock_outdated(stock["id"])
    else:
        # Scrape success; write to DB
        queries.write_stock_to_db(stock["id"], data["timestamp"], data["close"], data["prev_close"])

    # Trigger view
    tilename = tilename_for(stock)
    refresh_tile(tilename, "stock_view", stock["symbol"].strip())


def refresh_index(index):
    """
    Performs the complete cycle of updating a market index.
    1) calls newscraper to update data
    2) writes data to DB
    3) triggers views
    """
    try:
        # Call newscraper
        data = yahoo_scraper.get_index(index["symbol"])
    except:
        # Scrape failed; data is now outdated
        sys.stderr.write("The following error occurred while processing index {}:\n".format(index["symbol"]))
        traceback.print_exc()
        queries.set_index_outdated(index["id"])
    else:
        # Scrape success; write to DB
        queries.write_index_to_db(index["id"], data["timestamp"], data["close"], data["prev_close"])

    # Trigger view
    tilename = tilename_for(index)
    refresh_tile(tilename, "index_view", index["symbol"].strip())


def refresh_commodity(commodity):
    """
    Performs the complete cycle of updating a market index.
    1) calls newscraper to update data
    2) writes data to DB
    3) triggers views
    """
    try:
        # Call newscraper
        data = yahoo_scraper.get_commodity(commodity["symbol"])
    except:
        # Scrape failed; data is now outdated
        sys.stderr.write("The following error occurred while processing commodity {}:\n".format(commodity["symbol"]))
        traceback.print_exc()
        queries.set_commodity_outdated(commodity["id"])
    else:
        # Scrape success; write to DB
        queries.write_commodity_to_db(commodity["id"], data["timestamp"], data["close"], data["prev_close"])

    # Trigger view
    tilename = tilename_for(commodity)
    refresh_tile(tilename, "commodity_view", commodity["symbol"].strip())



if __name__ == "__main__":
    with Pool(4) as pool:
        pool.map(refresh_stock, queries.get_all_stocks())

    with Pool(4) as pool:
        pool.map(refresh_index, queries.get_all_indexes())

    with Pool(4) as pool:
        pool.map(refresh_commodity, queries.get_all_commodities())

    refresh_tile("bitcoin_tile", "bitcoin_view")

    print("Status: 302 Found")
    print("Location: /")
    print()
