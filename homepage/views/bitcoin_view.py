#! /usr/bin/env python

import sys
sys.path += ["/app/homepage/newscraper"]
import bitcoin  # pylint: disable=import-error

def get_data():
    ret = bitcoin.get_btc_info()
    return ret

def main():
    html = open("/app/homepage/views/bitcoin.html").read()
    return html.format(**get_data())

if __name__ == "__main__":
   print(main())
