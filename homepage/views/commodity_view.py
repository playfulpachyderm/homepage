#! /usr/bin/env python

import sys
import jinja2

sys.path += ["/app/homepage/db"]
import queries  # pylint: disable=import-error

from stock_view import format_data

def main(data):
    """Render and return the template"""
    html = jinja2.Template(open("/app/homepage/views/commodity.html").read())
    return html.render(**data)


if __name__ == "__main__":
    commodity_symbol = sys.argv[1]
    data = format_data(queries.get_commodity(commodity_symbol))
    print(main(data))
