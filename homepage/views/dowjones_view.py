#! /usr/bin/env python

import sys
sys.path += ["/app/homepage/newscraper"]
import dowjones

html = open("/app/homepage/views/dowjones.html").read()

data = dowjones.get_data("^DJI")
data["change"] = data["close"] - data["prev_close"]

print(html.format(**data))
