#! /usr/bin/env python

import datetime
import sys
import jinja2

sys.path += ["/app/homepage/db"]
import queries  # pylint: disable=import-error


def format_data(record):
    """Process and format data"""
    data = dict()
    data["name"] = record[0].strip()
    data["symbol"] = record[1]
    data["bought_at"] = record[2]
    data["currency"] = "" if record[3] == "CAD" else " ({})".format(record[3])
    data["latest_price"] = record[4]
    data["last_scraped"] = record[5]
    data["prev_close"] = record[6]
    data["outdated"] = record[7]

    data["change"] = data["latest_price"] - data["prev_close"]
    data["direction_of_change"] = "green" if data["change"] > 0 else "red"
    data["change_from_purchase"] = data["latest_price"] - data["bought_at"]
    data["direction_of_purchase_change"] = "green" if data["change_from_purchase"] > 0 else "red"
    data["formatted_timestamp"] = datetime.date.strftime(datetime.datetime.fromtimestamp(data["last_scraped"]), "%b %d, %Y at %H:%M")
    return data

def main(data):
    """Render and return the template"""
    html = jinja2.Template(open("/app/homepage/views/stock.html").read())
    return html.render(**data)


if __name__ == "__main__":
    stock_symbol = sys.argv[1]
    data = format_data(queries.get_stock(stock_symbol))
    print(main(data))
