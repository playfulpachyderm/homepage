#! /usr/bin/env python

import datetime

import sys
sys.path += ["/app/homepage/db"]
import queries  # pylint: disable=import-error
import jinja2


def format_data(record):
    data = dict()
    data["name"] = record[0]
    data["symbol"] = record[1]
    data["currency"] = "" if record[2] == "CAD" else " ({})".format(record[2])
    data["latest_price"] = record[3]
    data["last_scraped"] = record[4]
    data["prev_close"] = record[5]
    data["outdated"] = record[6]
    data["logo"] = record[7]

    data["change"] = data["latest_price"] - data["prev_close"]
    data["direction_of_change"] = "green" if data["change"] > 0 else "red"
    data["formatted_timestamp"] = datetime.date.strftime(datetime.datetime.fromtimestamp(data["last_scraped"]), "%b %d, %Y at %H:%M")
    return data


def main(data):
    html = jinja2.Template(open("/app/homepage/views/index_view.html").read())
    return html.render(**data)

if __name__ == "__main__":
    symbol = sys.argv[1]
    data = format_data(queries.get_index(symbol))
    print(main(data))
