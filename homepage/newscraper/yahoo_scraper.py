#!/usr/bin/env python

"""
Yahoo Scraper
-------------

Only `get_stock`, `get_index` and `get_commodity` should be used.
"""

import urllib.request as request
import time
import bs4

BASE_URL = "https://finance.yahoo.com/quote/{symbol}"

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0"
}

def selector(id, type="span"):
    return "{type}[data-reactid='{}']".format(id, type=type)

def clean(input, T=float):
    return T(input.replace(",", ""))


def get_page(symbol):
    url = BASE_URL.format(symbol=symbol)
    req = request.Request(url, headers=headers)
    resp = request.urlopen(req).read().decode()
    soup = bs4.BeautifulSoup(resp, "html.parser")
    return soup


def tbody_to_data(tbody):
    ret = {}

    for row in tbody.select("tr"):
        tds = row.select("td")
        assert len(tds) == 2, "Weird row! ({}).  {}".format(len(tds), row)
        ret[tds[0].text] = tds[1].text

    return ret


def list_tbodies_on_page(soup):
    table_div = soup.select("#quote-summary")[0]
    return table_div.select("tbody")


def full_data_for(symbol):
    soup = get_page(symbol)
    tbodies = list_tbodies_on_page(soup)

    data = {}
    for tbody in tbodies:
        data.update(tbody_to_data(tbody))

    current_price = soup.select(".Fz\(36px\)")
    assert len(current_price) == 1, (len(current_price), current_price)

    data["close"] = current_price[0].text

    return data

def get_stock(symbol="WHF"):
    data = full_data_for(symbol)

    return {
        "open": clean(data["Open"]),
        "close": clean(data["close"]),
        "prev_close": clean(data["Previous Close"]),
        "low": clean(data["Day's Range"].split("-")[0]),
        "high": clean(data["Day's Range"].split("-")[1]),
        "volume": clean(data["Volume"], T=int),
        "timestamp": time.time()
    }


def get_index(symbol="^DJI"):
    data = full_data_for(symbol)

    return {
        "open": clean(data["Open"]),
        "close": clean(data["close"]),
        "prev_close": clean(data["Previous Close"]),
        "low": clean(data["Day's Range"].split("-")[0]),
        "high": clean(data["Day's Range"].split("-")[1]),
        "volume": clean(data["Volume"], T=int),
        "timestamp": time.time()
    }


def get_commodity(symbol="GC=F"):
    data = full_data_for(symbol)

    return {
        "open": clean(data["Open"]),
        "close": clean(data["close"]),
        "prev_close": clean(data["Last Price"]),
        "low": clean(data["Day's Range"].split("-")[0]),
        "high": clean(data["Day's Range"].split("-")[1]),
        "volume": clean(data["Volume"], T=int),
        "timestamp": time.time()
    }


if __name__ == "__main__":
    import json

    for symbol in ["WHF", "SPY", "T.TO", "FTN.TO", "SRLP", "AINV"]:
        data = get_stock(symbol)
        print(json.dumps(data, indent=4))

    for symbol in ["^DJI", "000001.SS"]:
        data = get_index(symbol)
        print(json.dumps(data, indent=4))

    for symbol in ["GC=F", "CL=F"]:
        data = get_commodity(symbol)
        print(json.dumps(data, indent=4))
