#!/usr/bin/env python

import urllib.request as request
import json
import time
import datetime
from api_error import APIError

FREE_API_KEY = "DRV45WPIVCBVBJND"
function = "TIME_SERIES_DAILY"
interval = "1min"
url_base = "https://www.alphavantage.co/query?apikey={api_key}&function={function}&symbol={{symbol}}".format(
    api_key=FREE_API_KEY,
    function=function,
)

func_key = "Time Series (Daily)"

def current_date():
    return datetime.date.today()

def expected_timestamp(todays_date):
    """
    Returns a string in format: YYYY-mm-dd
    If today is Saturday or Sunday, expect data from Friday.  Otherwise it should be today.
    """
    if todays_date.weekday() > 4:
        # 0 => monday; 5 => saturday; 6 => sunday
        delta = todays_date.weekday() - 4
        todays_date -= datetime.timedelta(days=delta)
    return todays_date.strftime("%Y-%m-%d")

def remap(crap_keys):
    """
    The AlphaVantage API uses the most appalling keys.
    This function shouldn't change or remove any data, but it should give the keys nicer names.
    """
    return {
        "open": round(float(crap_keys["1. open"]), 2),
        "close": round(float(crap_keys["4. close"]), 2),
        "high": round(float(crap_keys["2. high"]), 2),
        "low": round(float(crap_keys["3. low"]), 2),
        "volume": round(float(crap_keys["5. volume"]), 2)
    }

def get_data(symbol="^DJI"):
    """
    Returns json-like data about the requested symbol.
    Fails with HTTPError if service is down (should be handled by caller).
    Fails with APIError if we are being throttled for too many requests, or if response is malformed.
    """
    today = expected_timestamp(current_date())
    url = url_base.format(symbol=symbol)
    resp = json.loads(request.urlopen(url).read().decode())
    if "Information" in resp or "Note" in resp:
        raise APIError("API overloaded [{}]".format(resp))

    try:
        quote = resp[func_key]
    except KeyError as e:
        raise APIError("API request failed (url: {}; tried for date {}).\nOutput:\n{}".format(url, today, resp)) from e
    latest = sorted(quote, reverse=True)[0]
    if today != latest:
        raise APIError("Expected date was not in result set")

    ret = remap(quote[today])
    prev_key = sorted(quote, reverse=True)[1]
    ret["prev_close"] = round(float(resp[func_key][prev_key]["4. close"]), 2)
    try:
        timestamp = time.strptime(resp["Meta Data"]["3. Last Refreshed"], "%Y-%m-%d %H:%M:%S")
    except ValueError as e:
        raise APIError("It's probably the weekend") from e
    ret["timestamp"] = time.mktime(timestamp)
    return ret


if __name__ == "__main__":
    stocks = get_data()
    print(json.dumps(stocks, indent=4))
