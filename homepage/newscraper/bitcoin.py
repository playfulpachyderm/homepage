#!/usr/bin/env python

"""More info: https://apiv2.bitcoinaverage.com/#price-data"""
import json
import urllib.request as request


def get_btc_info(ticker="BTCUSD"):
    r = request.urlopen("https://apiv2.bitcoinaverage.com/indices/global/ticker/{}".format(ticker))
    return json.loads(r.read().decode())

if __name__ == "__main__":
    btc_data = get_btc_info()

    for field in ("last", "low", "high", "ask", "bid", "timestamp"):
        assert field in btc_data
    for field in ("last", "low", "high"):
        print("{}: {}".format(field, btc_data[field]))
