import psycopg2


def db_connect():
    """Helper function that returns a DB connection"""
    return psycopg2.connect(dbname="homepage", user="www-data", password="i am breitbart", host="localhost")

def write_stock_to_db(stock_id, timestamp, close, prev_close):
    """Writes the result of a scrape to the DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare update_stock (int, real, real) as " +
                """UPDATE stocks
                      set last_scraped = to_timestamp($2),
                          latest_price = $3,
                          prev_close = $4,
                          outdated = false
                    where id=$1""")
            cur.execute("execute update_stock({}, {}, {}, {})".format(
                stock_id, timestamp, close, prev_close
            ))
    db_conn.close()


def set_stock_outdated(stock_id):
    """
    A stock is outdated if for any reason the data is more than 5 minutes old.
    This can be because a scrape failed, or the market isn't open right now.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare outdate_stock (int) as " +
                """UPDATE stocks
                      set outdated = true
                    where id=$1""")
            cur.execute("execute outdate_stock({})".format(stock_id))
    db_conn.close()


def write_index_to_db(index_id, timestamp, close, prev_close):
    """Writes the result of a scrape to the DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare update_index (int, real, real) as " +
                """UPDATE indexes
                      set last_scraped = to_timestamp($2),
                          latest_price = $3,
                          prev_close = $4,
                          outdated = false
                    where id=$1""")
            cur.execute("execute update_index({}, {}, {}, {})".format(
                index_id, timestamp, close, prev_close
            ))
    db_conn.close()


def set_index_outdated(index_id):
    """
    A stock is outdated if for any reason the data is more than 5 minutes old.
    This can be because a scrape failed, or the market isn't open right now.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare outdate_index (int) as " +
                """UPDATE indexes
                      set outdated = true
                    where id=$1""")
            cur.execute("execute outdate_index({})".format(index_id))
    db_conn.close()


def write_commodity_to_db(commodity_id, timestamp, close, prev_close):
    """Writes the result of a scrape to the DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare update_commodity (int, real, real) as " +
                """UPDATE commodities
                      set last_scraped = to_timestamp($2),
                          latest_price = $3,
                          prev_close = $4,
                          outdated = false
                    where id=$1""")
            cur.execute("execute update_commodity({}, {}, {}, {})".format(
                commodity_id, timestamp, close, prev_close
            ))
    db_conn.close()


def set_commodity_outdated(commodity_id):
    """
    A stock is outdated if for any reason the data is more than 5 minutes old.
    This can be because a scrape failed, or the market isn't open right now.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare outdate_commodity (int) as " +
                """UPDATE commodities
                      set outdated = true
                    where id=$1""")
            cur.execute("execute outdate_commodity({})".format(commodity_id))
    db_conn.close()

def get_all_stocks():
    """
    A list (generator) of all the data in the `stocks` table.
    Stocks are returned as dicts.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("SELECT id, name, symbol, bought_at, currency, latest_price, last_scraped from stocks order by id")
            while cur.rownumber < cur.rowcount:
                yield dict(zip(["id", "name", "symbol", "bought_at", "currency", "latest_price", "last_scraped"], cur.fetchone()))

    db_conn.close()

def get_all_indexes():
    """
    A list (generator) of all the data in the `indexes` table.
    Stocks are returned as dicts.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("SELECT id, name, symbol, currency, latest_price, last_scraped, logo from indexes order by id")
            while cur.rownumber < cur.rowcount:
                yield dict(zip(["id", "name", "symbol", "currency", "latest_price", "last_scraped", "logo"], cur.fetchone()))

    db_conn.close()

def get_all_commodities():
    """
    A list (generator) of all the data in the `commodities` table.
    Stocks are returned as dicts.
    """
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("SELECT id, name, symbol, bought_at, currency, latest_price, last_scraped from commodities order by id")
            while cur.rownumber < cur.rowcount:
                yield dict(zip(["id", "name", "symbol", "bought_at", "currency", "latest_price", "last_scraped"], cur.fetchone()))

    db_conn.close()

def get_stock(symbol):
    """Read from DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare find_stock (text) as " +
                """SELECT stock.name, symbol, bought_at, currency.name currency, latest_price, extract(epoch from last_scraped), prev_close, outdated from stocks stock join currency_codes currency on stock.currency = currency.id where symbol=$1""")
            cur.execute("execute find_stock('{}')".format(symbol))
            record = cur.fetchone()
    db_conn.close()
    return record

def get_index(symbol):
    """Read from DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare find_index (text) as " +
                """SELECT index.name, symbol, currency.name currency, latest_price, extract(epoch from last_scraped), prev_close, outdated, logo from indexes index join currency_codes currency on index.currency = currency.id where symbol=$1""")
            cur.execute("execute find_index('{}')".format(symbol))
            record = cur.fetchone()
    db_conn.close()
    return record

def get_commodity(symbol):
    """Read from DB"""
    with db_connect() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute("prepare find_commodity (text) as " +
                """SELECT commodity.name, symbol, bought_at, currency.name currency, latest_price, extract(epoch from last_scraped), prev_close, outdated from commodities commodity join currency_codes currency on commodity.currency = currency.id where symbol=$1""")
            cur.execute("execute find_commodity('{}')".format(symbol))
            record = cur.fetchone()
    db_conn.close()
    return record
