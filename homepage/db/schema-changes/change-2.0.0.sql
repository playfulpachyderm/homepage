alter table currency_codes add unique (name);
alter table currency_codes alter column name type varchar(3);

insert into currency_codes (name) values ('CNY');

alter table stocks alter column name type varchar(30);
alter table stocks add unique(name);
alter table stocks alter column symbol type varchar(10);
alter table stocks add unique(symbol);
alter table stocks alter column bought_at set not null;
alter table stocks add column prev_close real;
alter table stocks add column latest_price real;
alter table stocks add column last_scraped timestamp with time zone;
alter table stocks add column outdated boolean not null default false;

update stocks set symbol = 'T.TO' where symbol = 'TSE:T';
update stocks set last_scraped=now(), prev_close=0, latest_price=0 where symbol='T.TO';
update stocks set last_scraped=now(), prev_close=0, latest_price=0 where symbol='WHF';
insert into stocks (name, symbol, bought_at, currency, last_scraped, prev_close, latest_price) values ('Financial 15 Split Corp.', 'FTN.TO', 8.35, 1, now(), 0, 0);


create table commodities (id serial primary key,
	name varchar(30) not null unique,
	symbol varchar(10) not null unique,
	bought_at real not null,
	prev_close real,
	latest_price real,
	last_scraped timestamp with time zone,
	currency integer references currency_codes not null,
	outdated boolean not null default false
);

insert into commodities (name, symbol, bought_at, currency) values ('Gold', 'GC=F', 1234, 2);
insert into commodities (name, symbol, bought_at, currency) values ('Oil', 'CL=F', 71, 2);

update stocks set symbol = 'T.TO' where symbol = 'TSE:T';

create table indexes (id serial primary key,
	name varchar(50) not null unique,
	symbol varchar(10) not null unique,
	prev_close real,
	latest_price real,
	last_scraped timestamp with time zone,
	currency integer references currency_codes not null,
	outdated boolean not null default false,
	logo varchar(100) not null unique
);

insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('Dow Jones Industrial Average', '^DJI', 2, now(), 0, 'dowjones-logo.png');
insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('S&P 500', '^GSPC', 2, now(), 0, 'standard-poors.png');
insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('SSE (Shanghai) Composite Index', '000001.SS', 3, now(), 0, 'SSE-logo.png');
