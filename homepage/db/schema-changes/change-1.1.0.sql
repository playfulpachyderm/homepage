create table visit_count (id serial primary key,
	visits integer not null
);

insert into visit_count (visits) values (0);


create table currency_codes (id serial primary key,
	name char(3) not null
);

insert into currency_codes (name) values ('CAD');
insert into currency_codes (name) values ('USD');


create table stocks (id serial primary key,
	name char(30) not null,
	symbol char(10) not null,
	bought_at real,
	currency integer references currency_codes not null
);

insert into stocks (name, symbol, bought_at, currency) values ('Telus', 'TSE:T', 45.96, 1);
insert into stocks (name, symbol, bought_at, currency) values ('WHF', 'WHF', 12.33, 2);
