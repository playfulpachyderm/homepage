create table visit_count (id serial primary key,
	visits integer not null
);

insert into visit_count (visits) values (0);


create table currency_codes (id serial primary key,
	name varchar(3) not null unique
);

insert into currency_codes (name) values ('CAD');
insert into currency_codes (name) values ('USD');
insert into currency_codes (name) values ('CNY');


create table stocks (id serial primary key,
	name varchar(30) not null unique,
	symbol varchar(10) not null unique,
	bought_at real not null,
	prev_close real,
    latest_price real,
    last_scraped timestamp with time zone,
	currency integer references currency_codes not null,
    outdated boolean not null default false
);

insert into stocks (name, symbol, bought_at, currency, last_scraped, prev_close, latest_price) values ('Telus', 'T.TO', 45.96, 1, now(), 0, 0);
insert into stocks (name, symbol, bought_at, currency, last_scraped, prev_close, latest_price) values ('WHF', 'WHF', 12.33, 2, now(), 0, 0);
insert into stocks (name, symbol, bought_at, currency, last_scraped, prev_close, latest_price) values ('Financial 15 Split Corp.', 'FTN.TO', 8.35, 1, now(), 0, 0);


create table commodities (id serial primary key,
	name varchar(30) not null unique,
	symbol varchar(10) not null unique,
	bought_at real,
	prev_close real,
	latest_price real,
	last_scraped timestamp with time zone,
	currency integer references currency_codes not null,
	outdated boolean not null default false
);

insert into commodities (name, symbol, bought_at, currency) values ('Gold', 'GC=F', 1234, 2);
insert into commodities (name, symbol, bought_at, currency) values ('Oil', 'CL=F', 71, 2);

create table indexes (id serial primary key,
	name varchar(50) not null unique,
	symbol varchar(10) not null unique,
	prev_close real,
	latest_price real,
	last_scraped timestamp with time zone,
	currency integer references currency_codes not null,
	outdated boolean not null default false,
	logo varchar(100) not null unique
);

insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('Dow Jones Industrial Average', '^DJI', 2, now(), 0, 'dowjones-logo.png');
insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('S&P 500', '^GSPC', 2, now(), 0, 'standard-poors.png');
insert into indexes (name, symbol, currency, last_scraped, latest_price, logo) values ('SSE (Shanghai) Composite Index', '000001.SS', 3, now(), 0, 'SSE-logo.png');
